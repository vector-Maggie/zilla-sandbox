var fs = require('fs');

var rOption = {
	flags : 'r',
	encoding : null,
	mode : 0666
}

var wOption = {
	flags: 'w', // a追加 w覆盖
	encoding: null,
	mode: 0666   
}

var fileReadStream = fs.createReadStream('./1.txt', rOption);
var fileWriteStream = fs.createWriteStream('./2.txt', wOption);

fileReadStream.on('data', function(data) {
  fileWriteStream.write(data + '111111');
}); 

fileReadStream.on('error', function(err) {
  console.log('readStream' + err); 
  fileWriteStream.end();
});

fileReadStream.on('end', function() {
  console.log('readStream end'); 
  fileWriteStream.end();
});

fileReadStream.on('close', function(had_error) {
  console.log('readStream close'); 
  fileWriteStream.end();

  fs.rename('./1.txt', './1.txtOld', function(err) {
  	console.log(err);
  });
  fs.rename('./2.txt', './1.txt', function(err) {
    console.log(err);
  });
});
