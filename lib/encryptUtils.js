/**
 * AES加密
 * @param data
 * @param secretKey
 */
var crypto = require('crypto');

exports.aesEncrypt = function(data, secretKey) {
  var keyBuf = new Buffer(secretKey);
	var cipher = crypto.createCipher('aes-128-ecb', keyBuf);
	// return cipher.update(data,'utf8','hex') + cipher.final('hex');
  return cipher.update(data);
}

/**
 * aes解密
 * @param data
 * @param secretKey
 * @returns {*}
 */
exports.aesDecrypt = function(data, secretKey) {
  var keyBuf = new Buffer(secretKey);
	var cipher = crypto.createDecipher('aes-128-ecb', keyBuf);
	// return cipher.update(data, 'hex', 'utf8') + cipher.final('utf8');
  return cipher.update(data, 'binary', 'binary') + cipher.final('binary');
}