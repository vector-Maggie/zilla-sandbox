var fs = require('fs');
var streamHelper = require('../streamHelper.js');

/**
 * [fileHelper description] 文件夹递归
 * @param  {[type]} file_dir [description] 资源绝对路径
 * @key  {[type]} key [description] 加密用的key
 * @return {[type]}          [description]
 */
function fileHelper(file_dir, key, type) {
  if (fs.lstatSync(file_dir).isDirectory()) { // 判断目录
    var files = fs.readdirSync(file_dir);
    // 迭代
    files.forEach(function(item, index, array) {
      fileHelper(file_dir + '/' + item, key, type);
    });
  } else {
    // 处理文件
    streamHelper.readWriteStream(file_dir, file_dir + 'New', type, key);
  }
}

process.nextTick(function(){
  console.log('aaaa1');
});

// console.log(fs.lstatSync('e:/abc').isDirectory());
fileHelper('e:/abc', '5a987f8e1fb029', 0);

process.nextTick(function() {
  console.log('aaaa2');
});

// setInterval(function(){console.log("message");}, 10000);