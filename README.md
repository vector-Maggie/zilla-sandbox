#zilla-sandbox
	-- @email:heyaowen@foreveross.com-Vector Ho --
	-- 欢迎使用zilla-sandbox安全沙盒SDK，使用SDK请遵循以下指令规则 --
	    ps：确保资源内容编码为UTF8！AES对称方式加密解密，另特殊字符@勿用！
	| 1.加密文件：输入指令(ef@key@file)如：ef@abcdf0900@D:/sandbox.html |
	| 2.加密文件夹：输入指令(ed@key@path)如：ed@abcdf0900@D:/sandbox |
	| 3.解密文件：输入指令(格式df@key@file)如：df@abcdf0900@D:/sandbox.html |
	| 4.解密文件夹：输入指令(dd@key@path)如：dd@abcdf0900@D:/sandbox |
	| 5.生成加密列表文件：输入指令(fs@path)如：fs@D:/sandbox |
	| 6.退出SDK：输入指令：q |