/**
 * (@deprecated)
 */
var fs = require('fs');

var rOption = {
	flags : 'r',
	encoding : null,
	fd: null,
	mode : 0666,
	highWaterMark: 64 * 1024
}

var wOption = {
	flags: 'w', // a追加 w覆盖
	encoding: null,
	mode: 0666
}

/**
 * [createReadStream description] 读写流,复制文件，AES加密内容
 * @param  {[type]} sFile   [description]
 * @param  {[type]} dFile   [description]
 * @param  {[type]} secretKey   [description]
 * @param  {[type]} crypto   [description] 0 解密 1 加密 其他：不操作
 * @param  {[Function]} callback   [description] 操作完成回调
 * @return {[type]}         [description]
 */
exports.readWriteStream = function(sFile, dFile, crypto, secretKey, callback) {

	var fileReadStream = fs.createReadStream(sFile, rOption);
	var fileWriteStream = fs.createWriteStream(dFile, wOption);

	fileReadStream.on('data', function(data) {
		if (secretKey && crypto === 1) { // 加密
			console.log('正在加密...');
			// console.log(data instanceof Buffer); // true
			var buf = new Buffer(require('./encryptUtils.js').aesEncrypt(data, secretKey), 'binary');
		} else if(secretKey && crypto === 0) { // 解密
			console.log('正在解密...');
			try {
				data = require('./encryptUtils.js').aesDecrypt(data, secretKey);
			} catch(err) {
				console.log('\t请输入正确key解密！%s', err);
				// 删除类型为.txtNew的文件
				fs.unlink(dFile, function() {
					// console.log("正在清楚垃圾文件...");
					process.exit();
				});
			}
		}
		// 写入目标文件，目标文件如果不存在就创建
		fileWriteStream.write(buf);
	});

	fileReadStream.on('error', function(err) {
		console.log('readStream：\n' + err);
		fileWriteStream.end();
	});

	fileReadStream.on('end', function() {
	  // console.log('readStream end');
	  var buf = new Buffer(cipher.final('binary'), 'binary');
	  outputStream.write(buf);
	  outputStream.end();
	  outputStream.on('close', function() {
	    callback();
	  });
	});

	fileReadStream.on('close', function() {
		// console.log('readStream close');
		fileWriteStream.end();

		// 删除类型为.txtNew的文件
		fs.unlink(sFile, function() {
			fs.rename(dFile, sFile, function(err) {
					if (err) {
						console.log("rename dFile：" + err);
					}
					if(callback)
						callback(); // 回调喔～～～
				});
		});

		// 重新交换名称
		// fs.rename(sFile, sFile + 'Old', function(err) {
		// 	if (err) {
		// 		console.log("rename sFile：" + err);
		// 	} else {
		// 		fs.rename(dFile, sFile, function(err) {
		// 			if (err) {
		// 				console.log("rename dFile：" + err);
		// 			}
		// 			if(callback)
		// 				callback(); // 回调喔～～～
		// 		});
		// 	}
		// });

	});
};

/**
 * [createReadStream description] 写内容 生成文件
 * @param  {[type]} data   [description] 可以是String，又能使Buffer对象
 * @param  {[type]} path   [description] 文件夹路径
 * @return {[type]}         [description]
 */
exports.writeStream = function(data, path, callback) {
	var fileWriteStream = fs.createWriteStream(path + '/sandbox.json', wOption);
	// 写入目标文件，目标文件如果不存在就创建
	fileWriteStream.write(data, callback);
};