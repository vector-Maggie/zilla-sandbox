var packer = require('zip-stream');
var archive = new packer(); // OR new packer(options)

archive.on('error', function(err) {
  // throw err;
  console.log(err);
});

// pipe archive where you want it (ie fs, http, etc)
// listen to the destination's end, close, or finish event

archive.entry('string contents', { name: 'string.txt' }, function(err, entry) {
  if (err)
    throw err;
  archive.entry(null, { name: 'e:/1' }, function(err, entry) {
    if (err)
      throw err;
    archive.finalize();
  });
});