var fs = require('fs');

/*fs.readdir('E:/Git/forveveross/Chameleon/zilla-sandboxSDK', function(err, files) {
	var stat = fs.Stats();
	console.log();
	if(err) {
		console.log('检查资源是否存在？路径是否有问题？' + err);
	} else {
		console.log(files);
	}
});
*/

var rOption = {
	flags : 'r',
	encoding : null,
	mode : 0666
}

var wOption = {
	flags: 'w', // a追加 w覆盖
	encoding: null,
	mode: 0666   
}

var fileReadStream = fs.createReadStream('./1.txt', rOption);
var fileWriteStream = fs.createWriteStream('./2.txt', wOption);

fileReadStream.on('data', function(data) {
  console.log(data); // Buffer
  console.log(typeof(data));
  data = require('./encryptUtils.js').aesEncrypt(data, 'sdsadsad112');
  console.log(typeof(data));
  fileWriteStream.write(data);
 
    
});

fileReadStream.on('error', function(err) {
  console.log('readStream' + err); 
  fileWriteStream.end();
});

fileReadStream.on('end', function() {
  console.log('readStream end'); 
  fileWriteStream.end();
});

fileReadStream.on('close', function(had_error) {
  console.log('readStream close ' + had_error); 
  fileWriteStream.end();
  fs.rename('./1.txt', './1.txtOld', function(err) {
  	console.log(err);
  });
  fs.rename('./2.txt', './1.txt', function(err) {

  });
});
