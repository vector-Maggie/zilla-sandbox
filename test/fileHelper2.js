
var Encryptor = require('../fileEncryptor.js');
Encryptor = new Encryptor();

var key = '5a987f8e1fb029';

// Encrypt file.
// Encryptor.encrypt('abc.jpg', key, function(err) {
//   // Encryption complete.
// });

// Decrypt file.
Encryptor.decrypt('abc.jpg', key, function(err) {
  // Decryption complete.
});

// Encrypt file.
// Encryptor.encrypt('1.txt', key, function(err) {
//   // Encryption complete.
// });

 // Decrypt file.
Encryptor.decrypt('1.txt', key, function(err) {
  // Decryption complete.
});

